class ProductsController < ApplicationController
  def index
    @q = ProductVariation.ransack(params[:q])
    @product = @q.result.order('product_id ASC').paginate(page: params[:page], per_page: 10)
  end

  def new
    @product = Product.new
    @product.product_variations.build
    respond_to do |format|
      format.html { render :new }
      format.json { render Json: {status: :sucess, data: @product} }
    end
  end

  def edit
    @product = Product.find(params[:id])
    respond_to do |format|
      format.html { render :edit }
      format.json { render json: {status: :success, data: @product} }
    end
  end

  def create
    @product = Product.new(create_params)
    respond_to do |format|
      if @product.save
        format.html { redirect_to products_path, notice: 'Produto cadastrado com sucesso.'}
        format.json { render json: { status: :success, data: @product } }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end


  def update
    @product = Product.find(params[:id])
    respond_to do |format|
      if @product.update(update_params)
        format.html { redirect_to products_path, notice: 'Produto atualizado com sucesso.'}
        format.json { render json: { status: :success, data: @product } }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    status = false
    product_variation = ProductVariation.find(params[:id])
    @product = product_variation.product
    if (@product.product_variations.count <= 1)
      status = @product.destroy
    else
      status = product_variation.destroy_variation
    end
    respond_to do |format|
      if status
        format.html { redirect_to products_path, notice: 'Produto deletado com sucesso.'}
        format.json { head :no_content }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def create_params
    params.require(:product).permit(:name, :description,
      product_variations_attributes: [:id, :quantity, :size_id, :color_id, :_destroy])
  end

  def update_params
    params.require(:product).permit(:name, :description,
      product_variations_attributes: [:id, :quantity, :size_id, :color_id, :_destroy])
  end

end
