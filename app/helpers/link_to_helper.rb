module LinkToHelper

  def button_link_to(name = nil, options = nil, html_options = {}, &block)
    html_options[:class] ||= ''
    html_options[:class] = ' btn btn-primary ' + html_options[:class]
    link_to(name, options, html_options, &block)
  end

end
