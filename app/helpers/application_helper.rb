module ApplicationHelper

  def sizes
    Size.all
  end

  def colors
    Color.all
  end
end
