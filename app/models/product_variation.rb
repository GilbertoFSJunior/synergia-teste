class ProductVariation < ActiveRecord::Base

  #Relacionamentos------------------------------------------------------------------------------------------------------

  belongs_to :product
  belongs_to :size
  belongs_to :color



  #Validações-----------------------------------------------------------------------------------------------------------

  validates_presence_of :quantity, :color_id, :size_id
  validates_numericality_of :quantity, :only_integer => true, :greater_than_or_equal_to => 0

  def destroy_variation
    return self.destroy!
  end
end
