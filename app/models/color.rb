class Color < ActiveRecord::Base

  #Relacionamentos------------------------------------------------------------------------------------------------------

  has_many :product_variations

  #Validações-----------------------------------------------------------------------------------------------------------

  validates_presence_of :name, :hexadecimal

end
