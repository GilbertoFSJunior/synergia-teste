class Size < ActiveRecord::Base

  #Relacionamentos------------------------------------------------------------------------------------------------------

  has_many :product_variations

  #Validações-----------------------------------------------------------------------------------------------------------

  validates_presence_of :name, :abbreviation
end
