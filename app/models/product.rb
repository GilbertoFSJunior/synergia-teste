class Product < ActiveRecord::Base

  #Relacionamentos------------------------------------------------------------------------------------------------------

  has_many :product_variations, dependent: :delete_all
  accepts_nested_attributes_for :product_variations, reject_if: :all_blank, allow_destroy: true



  #Validações-----------------------------------------------------------------------------------------------------------

  validates_uniqueness_of :name
  validates_presence_of   :name, :description


end
