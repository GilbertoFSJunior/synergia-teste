class CreateProductVariations < ActiveRecord::Migration
  def change
    create_table :product_variations do |t|
      t.integer :quantity
      t.references :product, index: true, foreign_key: true
      t.references :size, index: true, foreign_key: true
      t.references :color, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
