# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts "Iniciando Criação padrão de tamanhos!"
Size.create! ([
    {name: "Tamanho 40 - 42", abbreviation: "PP"},
    {name: "Tamanho 44 - 46", abbreviation: "P"},
    {name: "Tamanho 48 - 50", abbreviation: "M"},
    {name: "Tamanho 52 - 54", abbreviation: "G"},
    {name: "Tamanho 56 - 58", abbreviation: "GG"},
    {name: "Tamanho 60 - 62", abbreviation: "XG"},
    {name: "Tamanho 64 - 66", abbreviation: "XGG"},
    {name: "Tamanho 68 - 70", abbreviation: "ZG"}
  ])
puts "Finalizando Criação padrão de tamanhos!"


puts "Iniciando Criação padrão de cores!"
Color.create! ([
    {name: "black",  hexadecimal: "#000000"},
    {name: "blue",   hexadecimal: "#0000FF"},
    {name: "green",  hexadecimal: "#008000"},
    {name: "brown",  hexadecimal: "#8B4513"},
    {name: "purple", hexadecimal: "#A020F0"},
    {name: "red",    hexadecimal: "#FF0000"},
    {name: "yellow", hexadecimal: "#FFFF00"},
    {name: "orange", hexadecimal: "#FFA500"}
])
puts "Finalizando Criação padrão de cores!"