require 'rails_helper'
describe Product do
  it "É válido quando nome e descrição estão presentes!" do
    product = Product.new(name: 'Camisa de botão',
                          description: 'Manga longa com estampa do one piece')
    expect(product).to be_valid
  end
end

describe Product do
  it "É invalido quando o nome é inexistente!" do
    product = Product.new(name: nil)
    product.valid?
    expect(product.errors[:name]).to include('Nome não pode ser nulo!')
  end
end

describe Product do
  it "É inválido caso já exista um mesmo nome ja cadastrado!" do
    product = Product.create(name: 'Camisa de botão',
                             description: 'Manga longa com estampa do one piece')
    product = Product.new(name: 'Camisa de botão',
                          description: 'Manga longa com estampa do one piece' )
    product.valid?
    expect(product.errors[:email]).to include('Nome não pode ser duplicado')
  end
end


#Fiz essa implemantação apenas para validar se o teste anterior realmente está sendo validado corretamente.
describe Product do
  it "É inválido caso já exista um mesmo nome ja cadastrado!" do
    product = Product.create(name: 'Camisa de botão',
                             description: 'Manga longa com estampa do one piece')
    product = Product.new(name: 'Camisa de botão',
                          description: 'Manga longa com estampa do one piece' )
    product.valid?
    expect(product.errors[:email]).not_to include('Nome não pode ser duplicado')
  end
end





