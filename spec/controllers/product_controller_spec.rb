require 'rails_helper'


RSpec.describe ProductsController, type: :routing do
  describe 'routing' do

    it 'rota para #index' do
      expect(get: '/products').to route_to('products#index')
    end

    it 'rota para #show' do
      expect(get: '/products/1').to route_to('products#show', id: '1')
    end

    it 'rota para #new' do
      expect(get: '/products/new').to route_to('products#new')
    end

    it 'rota para #update via PUT' do
      expect(put: '/products/1').to route_to('products#update', id: '1')
    end

    it 'rota para #update via PATCH' do
      expect(patch: '/products/1').to route_to('products#update', id: '1')
    end
  end
end

RSpec.describe ProductsController, :type => :controller do
  describe "GET index" do
    it "codigo 200" do
      get :index
      expect(response.status).to eq(200)
    end
  end
end

RSpec.describe ProductsController, :type => :controller do
  describe "GET new" do
    it "codigo 200" do
      get :new
      expect(response.status).to eq(200)
    end
  end
end


RSpec.describe ProductsController, :type => :controller do
  describe "responde" do
    it "responde ao html por padrão" do
      post :create, { :product => { :name => "Camisa do cruzeiro 2", :description => "O preço caiu!" } }
      expect(response.content_type).to eq "text/html"
    end
  end
end

RSpec.describe ProductsController, :type => :controller do
  describe "responde" do
    it "responde a formatos personalizados(json, etc..) quando fornecidos os parâmetros" do
      post :create, { :product => { :name => "Camisa do cruzeiro 3", :description => "O preço caiu!" }, :format => :json }
      expect(response.content_type).to eq "application/json"
    end
  end
end

RSpec.describe ProductsController, :type => :controller do
  describe "responde" do
    it 'create a new product' do
      params = {
          name: 'An awesome product',
          price: 50
      }
      expect { post(:create, params: { product: params }) }.to change(Product, :count).by(1)
      expect(flash[:notice]).to eq 'Product was successfully created.'
    end
  end
end